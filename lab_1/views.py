from django.shortcuts import render
from datetime import datetime, date
# Enter your name here
mhs_name = 'Yasmin Amalia' # TODO Implement this
curr_year = int(datetime.now().strftime("%Y"))
birth_date = date(1998, 3, 10) #TODO Implement this, format (Year, Month, Date)
npm = 1706043720 # TODO Implement this
uni = 'University of Indonesia'
hobi = 'Read novels, listen to music, travel.'
desc = "A tiny girl who's living in her huge wonderful world."

rach_name = 'Rachel Larasati'
rach_birth = date(1999, 8, 2)
rach_npm = 1706984713
rach_hobi = 'Listen to music'
rach_desc = 'Rachel is so expert at doing eyebrows makeup!'

comp = 'Monitor Acer V193HQV'
comp_birth = date(2009, 10, 12)
comp_npm = 1103
comp_hobi = 'Help the students code their program'
comp_desc = 'A not very slim computer monitor, its color is black, and works pretty well.'
# Create your views here.
def index(request):
    response = {'name': mhs_name, 'age': calculate_age(birth_date.year), 'npm': npm, 'uni': uni, 'hobi': hobi, 'desc': desc,
    'rach_name': rach_name, 'rach_birth': calculate_age(rach_birth.year), 'rach_npm': rach_npm,
    'rach_hobi': rach_hobi, 'rach_desc': rach_desc, 'comp': comp, 'comp_birth': calculate_age(comp_birth.year),
    'comp_hobi': comp_hobi, 'comp_desc': comp_desc}
    return render(request, 'index_lab1.html', response)

def calculate_age(birth_year):
    return curr_year - birth_year if birth_year <= curr_year else 0
